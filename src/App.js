import './App.css';
import { useRef, useState } from 'react';
import axios from 'axios';

function App() {

  const ref = useRef();

  const [objectUrl, setObjectUrl] = useState("");

  const [progressValue, setProgressValue] = useState(0);
  const [progressText, setProgressText] = useState("");
  const [progressLoaded, setProgressLoaded] = useState(0);
  const [progressTotal, setProgressTotal] = useState(0);

  const onSelectFile = e => {
    setObjectUrl(URL.createObjectURL(e.target.files[0]));

    const url = "https://api.cloudinary.com/v1_1/demo/image/upload";
    const file = e.target.files[0];
    const formData = new FormData();

    formData.append("file", file);
    formData.append("upload_preset", "docs_upload_example_us_preset");

    axios
      .post(url, formData, {
        onUploadProgress: progressEvent => {
          setProgressLoaded(progressEvent.loaded)
          setProgressTotal(progressEvent.total)
          let percentComplete = progressEvent.loaded / progressEvent.total
          percentComplete = parseInt(percentComplete * 90);
          console.log(percentComplete);
          setProgressText("上傳中...");
          setProgressValue(percentComplete)
        }
      })
      .then((result) => {
        setProgressValue(100)
        setProgressText("上傳完畢");
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      })

  };

  const onCancelFile = e => {
    ref.current.value = "";
    setObjectUrl("");
    setProgressText("");
    setProgressValue(0);
    setProgressLoaded(0);
    setProgressTotal(0);
  };

  return (
    <>
      <div className="App" style={{ backgroundColor: "#009BB4", height: "10vw", padding: "50px" }}>
        <h1>{`axios上傳圖片-進度條`}</h1>
        <input type="file" accept=".png,.jpeg" onChange={onSelectFile} ref={ref} ></input>
        {/* progress */}
        <label for="file">Downloading progress:</label>
        <progress id="file" value={progressValue} max="100"> </progress>
        {/* progress */}
        <div>{progressLoaded}/{progressTotal}{progressText}</div>
        <button onClick={onCancelFile}>取消</button>
      </div>
      <div style={{ display: "flex", justifyContent: "center", height: "10vw" }}>
        <div style={{ width: "60%", textAlign: "center" }}>
          <img src={objectUrl} alt="" width="80%" />
        </div>
      </div>
    </>
  );
}

export default App;
